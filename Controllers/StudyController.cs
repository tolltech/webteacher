﻿using System;
using System.IO;
using System.Net;
using Core.Sql;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebTeacher.Controllers
{

    //[MimeType("application/json")] Json,
    //[MimeType("application/x-protobuf")] Protobuf,
    //[MimeType("application/x-grobuf")] Grobuf,
    //[MimeType("application/xml")] Xml,
    //"Content-Type"

    [AllowAnonymous]
    public class StudyController : Controller
    {
        private readonly IJsonSerializer jsonSerializer;
        private readonly IXmlSerialiazer xmlSerialiazer;
        private readonly IQueryExecutorFactory queryExecutorFactory;
        private const string xmlContentType = "application/xml";
        private const string jsonContentType = "application/json";
        private static readonly ILog log = LogManager.GetLogger(typeof(StudyController));

        public StudyController(IJsonSerializer jsonSerializer, IXmlSerialiazer xmlSerialiazer, IQueryExecutorFactory queryExecutorFactory)
        {
            this.jsonSerializer = jsonSerializer;
            this.xmlSerialiazer = xmlSerialiazer;
            this.queryExecutorFactory = queryExecutorFactory;
        }

        public class Input
        {
            public int K { get; set; }
            public decimal[] Sums { get; set; }
            public int[] Muls { get; set; }
        }

        public class Output
        {
            public decimal SumResult { get; set; }
            public int MulResult { get; set; }
            public decimal[] SortedInputs { get; set; }
        }

        public void Ping()
        {
        }

        public void GetInputData()
        {
            WriteResponse(new Input
            {
                K = 10,
                Muls = new[] { 1, 4 },
                Sums = new[] { 1.01m, 2.02m }
            });
        }

        public void WriteAnswer()
        {
            var answer = GetFromBody<Output>();
        }

        [HttpGet]
        public ActionResult Help()
        {
            return PartialView();
        }

        [HttpGet]
        public string CalcMetronom(string time, int takts)
        {
            var split = time.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
            var min = split.Length == 2 ? int.Parse(split[0]) : 0;
            var sec = split.Length == 2 ? decimal.Parse(split[1]) : decimal.Parse(split[0]);
            var secs = min * 60 + sec;
            return (takts * 4 / (secs / 60)).ToString();
        }

        [HttpGet]
        public void Find(string key)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var keyValue = queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(key));
                WriteResponse(keyValue);
            }
        }

        [HttpGet]
        public void Select(string[] keys)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var keyValue = queryExecutor.Execute<IKeyValueHandler, KeyValue[]>(h => h.Select(keys));
                WriteResponse(keyValue);
            }
        }

        [HttpPost]
        public void Create()
        {
            var keyValue = GetFromBody<KeyValue>();
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                if (queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(keyValue.Key)) != null)
                    throw new HttpException((int)HttpStatusCode.BadRequest, string.Format("Key {0} is already presented in store.", keyValue.Key));
                queryExecutor.Execute<IKeyValueHandler>(h => h.Create(keyValue));
            }
        }

        [HttpPost]
        public void CreateAll()
        {
            var keyValues = GetFromBody<KeyValue[]>();
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                foreach (var keyValue in keyValues)
                {
                    if (queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(keyValue.Key)) != null)
                        throw new HttpException((int)HttpStatusCode.BadRequest, string.Format("Key {0} is already presented in store.", keyValue.Key));
                }
                queryExecutor.Execute<IKeyValueHandler>(h => h.Create(keyValues));
            }
        }

        [HttpPost]
        public void Update(string key, string value)
        {
            using (var queryExecutor = queryExecutorFactory.Create())
            {
                var existed = queryExecutor.Execute<IKeyValueHandler, KeyValue>(h => h.Find(key));
                if (existed == null)
                    throw new HttpException((int)HttpStatusCode.BadRequest, string.Format("Key {0} is not presented in store.", key));
                existed.Value = value;
                queryExecutor.Execute<IKeyValueHandler>(h => h.Update(existed));
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
                return;

            try
            {
                if (!(filterContext.Exception is HttpException))
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, "Oops...Somthing was wrong.", filterContext.Exception);
                }
                //throw filterContext.Exception;
            }
            catch (HttpException ex)
            {
                HttpContext.Response.StatusCode = ex.GetHttpCode();
                HttpContext.Response.Write(ex.Message);
                HttpContext.Response.End();
                log.Error(ex.Message, ex);
            }

            filterContext.ExceptionHandled = true;
            ControllerContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        private T GetFromBody<T>()
        {
            try
            {
                Request.InputStream.Seek(0, SeekOrigin.Begin);
                var body = Request.InputStream.ReadBytes();
                var contentType = Request.ContentType;
                return contentType == xmlContentType ? xmlSerialiazer.Deserialize<T>(body) : jsonSerializer.Deserialize<T>(body);
            }
            catch (Exception)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "Wrong parameters.");
            }
        }

        private void WriteResponse<T>(T response)
        {
            var realContentType = Request.AcceptTypes != null && Request.AcceptTypes.Contains(xmlContentType) ? xmlContentType : jsonContentType;
            var bytes = realContentType == xmlContentType ? xmlSerialiazer.Serialize(response) : jsonSerializer.Serialize(response);
            Response.AppendHeader("Content-Type", realContentType);
            Response.OutputStream.Write(bytes, 0, bytes.Length);
        }
    }
}